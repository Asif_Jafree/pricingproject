const toggle = document.getElementById('checkbox');
const professionalPrice = document.getElementById('professionalPrice');
const basicPrice = document.getElementById('basicPrice');
const masterPrice = document.getElementById('masterPrice');

const basicCourse = document.getElementById('basic');
const professionalCourse = document.getElementById('professional');
const masterCourse = document.getElementById('master');

basicCourse.addEventListener('mouseenter', () => {
  basicCourse.className = 'professional item';
  professionalCourse.className = 'basic item';
});

basicCourse.addEventListener('mouseleave', () => {
  basicCourse.className = 'basic item';
  professionalCourse.className = 'professional item';
});

masterCourse.addEventListener('mouseenter', () => {
  masterCourse.className = 'professional item';
  professionalCourse.className = 'basic item';
});

masterCourse.addEventListener('mouseleave', () => {
  masterCourse.className = 'basic item';
  professionalCourse.className = 'professional item';
});

const annualymonthly = (data) => {
  if (toggle.checked) {
    basicPrice.innerHTML = data.annually.basicPrice;
    professionalPrice.innerHTML = data.annually.professionalPrice;
    masterPrice.innerHTML = data.annually.masterPrice;
  } else {
    basicPrice.innerHTML = data.monthly.basicPrice;
    professionalPrice.innerHTML = data.monthly.professionalPrice;
    masterPrice.innerHTML = data.monthly.masterPrice;
  }
};

async function togglling() {
  const data = await fetch('data.json');
  const responseData = await data.json();
  toggle.addEventListener('change', () => {
    annualymonthly(responseData);
  });
}
togglling();
